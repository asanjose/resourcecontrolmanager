﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;




namespace ResourceControlManager.Pages
{


     public class Node
    {
        public string name;
        public string key;
        public string value;
        public DateTime TStamp;
    }

    public class PrivacyModel : PageModel
    {

   

        public PrivacyModel()
        {
           
        }
        public IList<Node> Nodes { get; set; }

        public IList<Node>  getNodeList(){
            return Singleton.GetNodes();
        }

        public async Task OnGetAsync()
        {

            Nodes = getNodeList();
        }
    }
}
