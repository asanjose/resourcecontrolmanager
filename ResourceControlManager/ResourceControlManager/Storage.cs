using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Newtonsoft.Json;
using RestSharp;
using System.Data;

using ResourceControlManager.Pages;

namespace ResourceControlManager
{
    public sealed class Singleton
    {
        private readonly static Singleton _instance = new Singleton();

       

        private Singleton()
        {
        }


        public class JsonItem
        {
            public string NAME { get; set; }
            public IList<Dictionary<string, string>> ITEMS { get; set; }
        }


        public static IList<Node> GetNodes()
        {

            var NodesList = new List<Node>();
            var client = new RestClient("http://localhost:5000");

            try
            {
                var request = new RestRequest("/getresources");
                var response = client.Post(request);

               // string json = @"[{'NAME':'NODO1', 'ITEMS': [{'key':'CPU','value':'80%'},{'key':'MEM','value':'70%'}] } , {'NAME':'NODO2', 'ITEMS': [{'key':'CPU','value':'80%'},{'key':'MEM','value':'70%'}]}]";

                List<JsonItem> nodes = JsonConvert.DeserializeObject<List<JsonItem>>(response.Content);

                foreach (var node_item in nodes)
                {
                   

                    foreach (var item in node_item.ITEMS)
                    {
                        Node result_node = new Node();
                        result_node.name = node_item.NAME;
                        result_node.key = item["key"];
                        result_node.value = item["value"];
                        result_node.TStamp = DateTime.Now;

                        NodesList.Add(result_node);

                    }

                }

       
          

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);

            }

            return NodesList;
        }


          public static Singleton Instance
        {
            get
            {
                return _instance;
            }
        }
    }
}
