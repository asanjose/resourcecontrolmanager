// <auto-generated>
//     Generated by the protocol buffer compiler.  DO NOT EDIT!
//     source: protos/healt.proto
// </auto-generated>
#pragma warning disable 0414, 1591
#region Designer generated code

using grpc = global::Grpc.Core;

namespace MyHealt {
  public static partial class Health
  {
    static readonly string __ServiceName = "health.Health";

    static readonly grpc::Marshaller<global::MyHealt.HealthCheckRequest> __Marshaller_health_HealthCheckRequest = grpc::Marshallers.Create((arg) => global::Google.Protobuf.MessageExtensions.ToByteArray(arg), global::MyHealt.HealthCheckRequest.Parser.ParseFrom);
    static readonly grpc::Marshaller<global::MyHealt.HealthCheckResponse> __Marshaller_health_HealthCheckResponse = grpc::Marshallers.Create((arg) => global::Google.Protobuf.MessageExtensions.ToByteArray(arg), global::MyHealt.HealthCheckResponse.Parser.ParseFrom);

    static readonly grpc::Method<global::MyHealt.HealthCheckRequest, global::MyHealt.HealthCheckResponse> __Method_Check = new grpc::Method<global::MyHealt.HealthCheckRequest, global::MyHealt.HealthCheckResponse>(
        grpc::MethodType.Unary,
        __ServiceName,
        "Check",
        __Marshaller_health_HealthCheckRequest,
        __Marshaller_health_HealthCheckResponse);

    /// <summary>Service descriptor</summary>
    public static global::Google.Protobuf.Reflection.ServiceDescriptor Descriptor
    {
      get { return global::MyHealt.HealtReflection.Descriptor.Services[0]; }
    }

    /// <summary>Base class for server-side implementations of Health</summary>
    [grpc::BindServiceMethod(typeof(Health), "BindService")]
    public abstract partial class HealthBase
    {
      public virtual global::System.Threading.Tasks.Task<global::MyHealt.HealthCheckResponse> Check(global::MyHealt.HealthCheckRequest request, grpc::ServerCallContext context)
      {
        throw new grpc::RpcException(new grpc::Status(grpc::StatusCode.Unimplemented, ""));
      }

    }

    /// <summary>Creates service definition that can be registered with a server</summary>
    /// <param name="serviceImpl">An object implementing the server-side handling logic.</param>
    public static grpc::ServerServiceDefinition BindService(HealthBase serviceImpl)
    {
      return grpc::ServerServiceDefinition.CreateBuilder()
          .AddMethod(__Method_Check, serviceImpl.Check).Build();
    }

    /// <summary>Register service method with a service binder with or without implementation. Useful when customizing the  service binding logic.
    /// Note: this method is part of an experimental API that can change or be removed without any prior notice.</summary>
    /// <param name="serviceBinder">Service methods will be bound by calling <c>AddMethod</c> on this object.</param>
    /// <param name="serviceImpl">An object implementing the server-side handling logic.</param>
    public static void BindService(grpc::ServiceBinderBase serviceBinder, HealthBase serviceImpl)
    {
      serviceBinder.AddMethod(__Method_Check, serviceImpl == null ? null : new grpc::UnaryServerMethod<global::MyHealt.HealthCheckRequest, global::MyHealt.HealthCheckResponse>(serviceImpl.Check));
    }

  }
}
#endregion
